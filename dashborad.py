import dash
import dash_table
import dash_core_components as dcc
import dash_html_components as html
import pandas as pd
import pickle
import pprint
import io
import requests
import json
import plotly.express as px
import plotly.graph_objects as go
import numpy as np
import xlrd
from  geopy.geocoders import Nominatim

external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']
app = dash.Dash(__name__, external_stylesheets=external_stylesheets)
encodes = ['utf-8', 'Big5']

# read csv urls
with open('./docs/record.pickle', 'rb') as handle:
    titles_urls = pickle.load(handle)
titles_urls = pd.DataFrame.from_dict(titles_urls, orient='index')
titles = titles_urls['title'].tolist()

# the function to find lat and lon （尚未完成)
geolocator = Nominatim(user_agent="testing", timeout=3)
def lat_lon(addresses):
    lats = []
    lons = []
    for address in addresses:
        loc = geolocator.geocode(address)
        while loc is None:
            address = address[:-1]
            loc = geolocator.geocode(address)
        lat = loc.latitude
        lon = loc.longitude
        lats.append(lat)
        lons.append(lon)
    return lats, lons

# the app layout
app.layout = html.Div([
    html.Div([
        # select which gov data
        html.H1(children='Government Data Visualization'),
        html.H4(children='選擇要使用的政府資料'),
        html.Div(
            [dcc.Dropdown(
                id='which_gov_data',
                options=[{'label': str(title), 'value': str(title)} for title in titles],
                value='西藏統計資料',
                style={'width': '50%'}
            ),
            dcc.Dropdown(
                id='encoding',
                options=[{'label': str(encode), 'value': str(encode)} for encode in encodes],
                value='utf-8',
                style={'width': '50%'}
            ),
        ]),
        # the place to save df
        html.Div(
            id='save_df_json',
            style={'display': 'none'}
        ),
        # the place to display table
        html.H6(children='前幾列資料'),
        html.Div(
            id='table'
        )
    ]),

    html.Div([
        # draw the scatter plot
        html.H4(children='scatter plot'),
        html.P(children='任選兩欄作為x與y軸，並選擇一欄做為input資料'),
        # the inputs to set scatter plot
        html.Div([
            html.P(children='Select the x axis of scatter plot'),
            dcc.Dropdown(
                id='crossfilter-xaxis-column',
            ),
            html.P(children='Select the y axis of scatter plot'),
            dcc.Dropdown(
                id='crossfilter-yaxis-column',
            ),
            html.P(children='Select the input of scatter plot'),
            dcc.Dropdown(
                id='crossfilter-zaxis-column',
            )], style={'width': '49%', 'display': 'inline-block'}),
        dcc.Graph(
            id='crossfilter-indicator-scatter',
        style={'width': '49%', 'float': 'right', 'display': 'inline-block'})
    ]),

    html.Div([
        # map plot
        html.H4(children='map plot'),
        html.P(children='選擇一欄緯度，一欄經度，並選擇一欄做為input資料 (之後會新增一個計算地址經緯度的流程)'),
        # the input for maps
        html.P(children='Select the address'),
        dcc.Dropdown(
            id='address',
        ),
        html.P(children='Select the latitude'),
        dcc.Dropdown(
            id='latitude',
        ),
        html.P(children='Select the longitude'),
        dcc.Dropdown(
            id='longitude',
        ),
        html.P(children='Select the input of map plot'),
        dcc.Dropdown(
            id='text',
        ),
        dcc.Graph(id='map')
    ],style={'width': '100%', 'display': 'inline-block'}),
])

## the callbacks
# callbacks for which_gov_data
@app.callback(
    [dash.dependencies.Output('table', 'children'),
    dash.dependencies.Output('save_df_json', 'children'),
    dash.dependencies.Output('crossfilter-xaxis-column', 'options'),
    dash.dependencies.Output('crossfilter-yaxis-column', 'options'),
    dash.dependencies.Output('crossfilter-zaxis-column', 'options'),
    dash.dependencies.Output('address', 'options'),
    dash.dependencies.Output('latitude', 'options'),
    dash.dependencies.Output('longitude', 'options'),
    dash.dependencies.Output('text', 'options')],
    [dash.dependencies.Input('which_gov_data', 'value'),
    dash.dependencies.Input('encoding', 'value')])
def update_which_gov_data(gov_table_name, encoding, max_rows=10):
    url = titles_urls.loc[titles_urls['title'] == gov_table_name].iloc[0]['url']
    url_content = requests.get(url, verify=False).content
    try:
        df = pd.read_excel(io.StringIO(url_content.decode(str(encoding), 'ignore')), error_bad_lines=False)
    except:
        df = pd.read_csv(io.StringIO(url_content.decode(str(encoding), 'ignore')), error_bad_lines=False)
    try:
        available_indicators = df.columns.tolist()
        return [
            html.Table(
                # Header
                [html.Tr([html.Th(col) for col in df.columns]) ] +
                # Body
                [html.Tr([
                    html.Td(df.iloc[i][col]) for col in df.columns
                ]) for i in range(min(len(df), max_rows))]
            ),
            df.to_json(date_format='iso', orient='split'),
            [{'label': str(i), 'value': str(i)} for i in available_indicators],
            [{'label': str(i), 'value': str(i)} for i in available_indicators],
            [{'label': str(i), 'value': str(i)} for i in available_indicators],
            [{'label': str(i), 'value': str(i)} for i in available_indicators],
            [{'label': str(i), 'value': str(i)} for i in available_indicators],
            [{'label': str(i), 'value': str(i)} for i in available_indicators],
            [{'label': str(i), 'value': str(i)} for i in available_indicators]
        ]
    except Exception as e:
        available_indicators = [None]
        return [
            html.Th("沒載到資料, error: " + str(e)),
            pd.DataFrame(np.array([None, None, None]).reshape(-1,3), 
                columns=['沒','載','到']).to_json(date_format='iso', orient='split'),
            [{'label': str(i), 'value': str(i)} for i in available_indicators],
            [{'label': str(i), 'value': str(i)} for i in available_indicators],
            [{'label': str(i), 'value': str(i)} for i in available_indicators],
            [{'label': str(i), 'value': str(i)} for i in available_indicators],
            [{'label': str(i), 'value': str(i)} for i in available_indicators],
            [{'label': str(i), 'value': str(i)} for i in available_indicators],
            [{'label': str(i), 'value': str(i)} for i in available_indicators]
        ]

# callbacks for 2D graph
@app.callback(
    dash.dependencies.Output('crossfilter-indicator-scatter', 'figure'),
    [dash.dependencies.Input('save_df_json', 'children'),
    # inputs for x, y, and data
    dash.dependencies.Input('crossfilter-xaxis-column', 'value'),
    dash.dependencies.Input('crossfilter-yaxis-column', 'value'),
    dash.dependencies.Input('crossfilter-zaxis-column', 'value')])
def update_graph(table_data, xaxis_column_name, yaxis_column_name, input_data):
    df = pd.read_json(table_data, orient='split')
    return {
        'data': [dict(
            x=df[xaxis_column_name],
            y=df[yaxis_column_name],
            # text=dff[dff['Indicator Name'] == yaxis_column_name]['Country Name'],
            customdata=df[input_data],
            mode='markers',
            marker={
                'size': 15,
                'opacity': 0.5,
                'line': {'width': 0.5, 'color': 'white'}
            }
        )],
        'layout': dict(
            xaxis={
                'title': xaxis_column_name,
                # 'type': 'linear' if xaxis_type == 'Linear' else 'log'
            },
            yaxis={
                'title': yaxis_column_name,
                # 'type': 'linear' if yaxis_type == 'Linear' else 'log'
            },
            margin={'l': 40, 'b': 30, 't': 10, 'r': 0},
            height=450,
            hovermode='closest'
        )
    }

# callbacks for map
@app.callback(
    # map plot
    dash.dependencies.Output('map', 'figure'),
    # inputs for table data * column name
    [dash.dependencies.Input('save_df_json', 'children'),
    dash.dependencies.Input('address', 'value'),
    dash.dependencies.Input('latitude', 'value'),
    dash.dependencies.Input('longitude', 'value'),
    dash.dependencies.Input('text', 'value')])
def update_graph(table_data, address_column, latitude, longitude, text):
    df = pd.read_json(table_data, orient='split')
    fig = go.Figure(go.Scattermapbox(
        lat=df[latitude],
        lon=df[longitude],
        mode='markers',
        marker=go.scattermapbox.Marker(
        # size=size
        ),
        text=text,
    ))
    fig.update_layout(mapbox_style="carto-positron")
    return fig

if __name__ == '__main__':
    app.run_server(debug=True)
