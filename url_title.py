import pprint
import pickle
import pandas as pd

# read csv urls
with open('./docs/record.pickle', 'rb') as handle:
    titles_urls = pickle.load(handle)
pp = pprint.PrettyPrinter(indent=4)
pp.pprint(titles_urls)
