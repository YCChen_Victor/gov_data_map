# gov_data_map

gov_data_map is to visualize the csv files from https://data.gov.tw/ with plotly dash (2020/0/18)

## Installation

```bash
pip install -r requirements.txt
```

## Usage

Please get the url of csv files from the project https://gitlab.com/YCChen_Victor/scrape_open_gov_data_tw then

```bash
python dashboard.py
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)


